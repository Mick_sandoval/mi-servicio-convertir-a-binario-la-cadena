import java.net.*;
import java.io.*;
import javax.xml.bind.DatatypeConverter;

public class MultiServerThread extends Thread {
   private Socket socket = null;

   public MultiServerThread(Socket socket) {
      super("MultiServerThread");
      this.socket = socket;
      ServerMultiClient.NoClients++;
   }

   public void run() {

      try {
         PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
         BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
         String lineIn, lineOut;
		
	    while((lineIn = entrada.readLine()) != null){
            System.out.println("Received: "+lineIn);
            escritor.flush();
            if(lineIn.equals("FIN")){
               ServerMultiClient.NoClients--;
                break;
            }else if(lineIn.equals("#clientes#")){
               escritor.println("Clientes conectados: "+ServerMultiClient.NoClients);
               escritor.flush();
            }else if(lineIn.charAt(0)=='#'){
                System.out.println("Entramos al servicio");
                if(lineIn.charAt(lineIn.length()-1)=='&'){
                    System.out.println("Entramos al if para checar sintaxis");
                    if(lineIn.charAt(1)=='6' && lineIn.charAt(2)=='2' && lineIn.charAt(3)=='2'&& lineIn.charAt(4)=='|'){
                        System.out.println("Entramos al if del codigo");
                        String subLine = lineIn.substring(5, lineIn.length()-1);
                        escritor.println(" "+ subLine);
                        
                         String  n,sn ="";
        int x=0;
        for (int i=0; i<subLine.length(); i++){
        x=subLine.charAt(i);
        n=Integer.toBinaryString(x);
        sn= sn+n;
        System.out.println(subLine.charAt(i));
        System.out.println(x);
        System.out.println(n+"\n");
        }
        escritor.println(" "+sn);
        escritor.flush();
                        }
                    else{
                        escritor.println("ERROR! ->codigo incorrecto<-");
                        escritor.flush();
                    }
                }else{
                    escritor.println("ERROR! ->sintaxis incorrecta<-");
                    escritor.flush();
                }
            }else{
                escritor.println("Echo... "+lineIn);
                escritor.flush();
            }
         } 
         try{		
            entrada.close();
            escritor.close();
            socket.close();
         }catch(IOException e){ 
            System.out.println ("Error : " + e.toString()); 
            socket.close();
            System.exit (0); 
   	   } 
      }catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   public static String toHexadecimal(String text) throws UnsupportedEncodingException{
        byte[] myBytes = text.getBytes("UTF-8");

        return DatatypeConverter.printHexBinary(myBytes);
    }
   
} 

